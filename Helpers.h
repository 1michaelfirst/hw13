#pragma once

long sum(long a, long b) {
	return a + b;
}

long sqrt(long x) {
	return x * x;
}

long sqrtSum(long a, long b) {
	return sqrt(sum(a, b));
}
